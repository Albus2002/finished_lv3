#pragma once
#include<iostream>
#include<cstring>
#include<memory>
#include<cassert>
#include "util.h"

using namespace std;

// CompUnit 是 BaseAST
class CompUnitAST : public BaseAST {
 public:
  // 用智能指针管理对象
  std::unique_ptr<BaseAST> func_def;
  KIR Dump() const override {
    string s = "fun ";
    string tmp = func_def->Dump().s;
    return KIR(s+tmp);
  }
};

// FuncDef 也是 BaseAST

class FuncDefAST : public BaseAST {
 public:
  std::unique_ptr<BaseAST> func_type;
  std::string ident;
  std::unique_ptr<BaseAST> block;
  KIR Dump() const override {
    if(ident == "main"){
        string s = "@"+ident+"(): " 
                   + func_type->Dump().s 
                   +"{\n"
                   +block->Dump().s
                   +"\n}";
        return KIR(s);
    }
    else{
        assert(0);
    }
  }
    
};

class FuncTypeAST : public BaseAST{
public:
 std::string func_type;
 KIR Dump() const override{
    string s = "";
    if(func_type=="int"){
        s += "i32 ";
    }
    return KIR(s);
 }

};

class BlockAST:public BaseAST{
public:
    std::unique_ptr<BaseAST> stmt;
    BlockAST() = default;
    KIR Dump() const override{
        string s = "%entry:\n"
                   +stmt->Dump().s;
        return KIR(s);
    }
};

class StmtAST : public BaseAST{
public:
    //std::unique_ptr<BaseAST> number;
    std::unique_ptr<BaseAST> matched_stmt;
    int kind;

    StmtAST() = default;
    KIR Dump() const override{
        if(debug){
            cout<<"building stmt"<<endl;
        }
        if(kind == 0){
            return matched_stmt->Dump();
        }
        else{
            return KIR("unknown stmt");
        }
    }
};

class MatchedStmtAST:public BaseAST{
    public:
    unique_ptr<BaseAST> exp;
    int kind;//kind = 0 is exp
    KIR Dump() const override{
        if(kind == 0){
            if(debug){
                cout<<"Matchedstmt, kind 0, exp"<<endl;
            }
            auto exp_dump = exp->Dump();
            string tmp_s = exp_dump.s;
            string tmp_var = exp_dump.tmp_var;
            return KIR(tmp_s + " ret " + tmp_var + "\n",1);//end of control stream
        }
        else{
            assert(0);
        }
    }
};

class ExpAST : public BaseAST{
public:
    unique_ptr<BaseAST> exp;
    KIR Dump() const override{
        if(debug){
            cout<<"ExpAST"<<endl;
        }
        return exp->Dump();
    }
};

class LOrExpAST : public BaseAST{
public:
    int kind;
    unique_ptr<BaseAST> lor_exp;// kind = 1,lor || land
    unique_ptr<BaseAST> land_exp;//kind = 0
    
    KIR generate_return() const{
        string true_label = new_label();
        string false_label = new_label();
        string end_label = new_label();
        string tmp_var_1 = "%" + to_string(tmp_var_cnt);
        tmp_var_cnt++;
        string s = " " + tmp_var_1 + " = alloc i32\n  store 1, " + tmp_var_1 + "\n";
        
        KIR lor_dump = lor_exp->Dump();
        KIR land_dump = land_exp->Dump();
        
        s += lor_dump.s;

        string tmp_var_2 = "%" + to_string(tmp_var_cnt);
        tmp_var_cnt++;
        s += "  " + tmp_var_2 + " = eq " + lor_dump.tmp_var + ", 0\n";
        s += "  br " + tmp_var_2 + ", " + true_label + ", " + false_label +"\n";
        
        s += true_label + ":\n";

        s += land_dump.s;
        string tmp_var_3 = "%" + to_string(tmp_var_cnt);
        tmp_var_cnt++;
        s += " " + tmp_var_3 + " = ne " + land_dump.tmp_var + ", 0\n";
        s += "  store " + tmp_var_3 + ", " +tmp_var_1 + "\n";
        
        string tmp_var_4 = "%" + to_string(tmp_var_cnt);
        tmp_var_cnt++;
        s += "  jump " + end_label + "\n" + false_label + ":\n  jump "+ end_label 
            + "\n" + end_label + ":\n";

        s += "  " + tmp_var_4 + " = load " + tmp_var_1 + "\n";
        return KIR(s,tmp_var_4);
    }
    KIR Dump() const override{
        if(kind == 0){
            if(debug){
                cout<<"lorexpAST, kind 0"<<endl;
            }
            return land_exp->Dump();
        }
        else if(kind == 1){
            if(debug){
                cout<<"lor||land,kind 1"<<endl;
            }
            return generate_return();
        }
        else{
            assert(false);
        }
    }
};

class LAndExpAST : public BaseAST{
public:
    int kind;
    unique_ptr<BaseAST> land_exp; // kind = 1, land && eq
    unique_ptr<BaseAST> eq_exp; // kind = 0
    
    KIR generate_return() const{
        string true_label = new_label();                                            
        string false_label = new_label();                                         
        string end_label = new_label();                                          
        string tmp_var_1 = "%" + to_string(tmp_var_cnt++);                    
        string s = "  " + tmp_var_1 + " = alloc i32\n  store 0, " + tmp_var_1 + "\n"; 

        KIR land_dump = land_exp->Dump();
        KIR eq_dump = eq_exp->Dump();

        s += land_dump.s; 

        string tmp_var_2 = "%" + to_string(tmp_var_cnt++);               
        s += "  " + tmp_var_2 + " = ne " + land_dump.tmp_var + ", 0\n"; 

        s += "  br " + tmp_var_2 + ", " + true_label + ", " + false_label
              + "\n";            
        s += true_label + ":\n";

        s += eq_dump.s;
        string tmp_var_3 = "%" + to_string(tmp_var_cnt++);               
        s += "  " + tmp_var_3 + " = ne " + eq_dump.tmp_var + ", 0\n"; 
        s += "  store " + tmp_var_3 + ", " + tmp_var_1 + "\n";            

        string tmp_var_4 = "%" + to_string(tmp_var_cnt++); 
        s += "  jump " + end_label + "\n" + false_label + ":\n  jump " + end_label + "\n"
              + end_label + ":\n";

        s += "  " + tmp_var_4 + " = load " + tmp_var_1 + "\n"; //%4=%1
        
        return KIR(s, tmp_var_4);
    }
    KIR Dump() const override{
        if(kind == 0){
            if(debug){
                cout<<"landexpAST, kind 0"<<endl;
            }
            return eq_exp->Dump();
        }
        else if(kind == 1){
            if(debug){
                cout<<"landexpAST, kind 1"<<endl;
            }
            return generate_return();
        }
        else{
            assert(0);
        }
    }
};

class EqExpAST : public BaseAST{
public:
    int kind;
    //0 rel,1 == ,2 !=
    unique_ptr<BaseAST> eq_exp;
    unique_ptr<BaseAST> rel_exp;
    KIR generate_return() const{
        KIR exp_dump_1 = eq_exp->Dump();
        string tmp_cmd_1 = exp_dump_1.s;
        string tmp_var_1 = exp_dump_1.tmp_var;
        KIR exp_dump_2 = rel_exp->Dump();
        string tmp_cmd_2 = exp_dump_2.s;
        string tmp_var_2 = exp_dump_2.tmp_var;
        string tmp_var_new = "%" + to_string(tmp_var_cnt++);
        if (debug) {
            cout << "CMD1: " << tmp_cmd_1<< endl;
            cout << "Var1: %" << tmp_var_1<< endl;
            cout << "CMD2: " << tmp_cmd_2 << endl;
            cout << "Var2: %" << tmp_var_2 << endl;
            cout << "VarNew: " << tmp_var_new << endl;
        }
        string op = " = eq ";
        if (kind == 1)
            op = " = eq ";
        else if (kind == 2)
            op = " = ne ";
        KIR ret = KIR(
            tmp_cmd_1 + tmp_cmd_2 + "  " + tmp_var_new + op + tmp_var_1 + ", " + tmp_var_2 + "\n", tmp_var_new);
        return ret;
    }
    KIR Dump() const override{
        if(kind == 0){
            if(debug){
                cout<<"eq_Exp_AST, kind 0"<<endl;
            }
            return rel_exp->Dump();
        }
        else if(kind == 1){
            if(debug){
                cout<<"eq_Exp_AST, kind 1"<<endl;
            }
            return generate_return();
        }
        else if(kind==2){
            if(debug){
                cout<<"eq_Exp_AST, kind 2"<<endl;
            }
            return generate_return();
        }
        else{
            assert(0);
        }
    }
};

class RelExpAST: public BaseAST {
public:
    int kind;
    // 0 add, 1 < ,2 > ,3 <=, 4 >=
    unique_ptr<BaseAST> rel_exp;
    unique_ptr<BaseAST> add_exp;

    KIR generate_return() const{
        KIR exp_dump_1 = rel_exp->Dump();
        string tmp_cmd_1 = exp_dump_1.s;
        string tmp_var_1 = exp_dump_1.tmp_var;
        KIR exp_dump_2 = add_exp->Dump();
        string tmp_cmd_2 = exp_dump_2.s;
        string tmp_var_2 = exp_dump_2.tmp_var;
        string tmp_var_new = "%" + to_string(tmp_var_cnt++);
        if(debug){
            cout << "tmp_cmd_1: " << tmp_cmd_1 << endl;
            cout << " tmp_var_1: " << tmp_var_1 << endl;
            cout << "tmp_cmd_2: " << tmp_cmd_2 << endl;
            cout << "tmp_var_2: " << tmp_var_2 << endl;
            cout << "tmp_var_new: " << tmp_var_new << endl;
        }
        string op = "";
        if(kind == 1){
            op = " = lt ";
        }
        else if(kind==2){
            op = " = gt ";
        }
        else if(kind==3){
            op = " = le ";
        }
        else if(kind == 4){
            op = " = ge ";
        }
        return KIR(tmp_cmd_1+tmp_cmd_2+" "+tmp_var_new+op+tmp_var_1+", "+tmp_var_2 +"\n",tmp_var_new);
    }
    KIR Dump() const override {
        if(kind==0){
            if(debug){
                cout << "relexpAST, kind 0" <<endl;
            }
            return add_exp->Dump();
        }
        else if(kind==1){
            if(debug){
                cout << "relexpAST, kind 1" <<endl;
            }
            return generate_return();
        }
        else if(kind == 2){
            if(debug){
                cout << "relexpAST, kind 2" <<endl;
            }
            return generate_return();
        }
        else if(kind==3){
            if(debug){
                cout << "relexpAST, kind 3" <<endl;
            }
            return generate_return();
        }
        else if(kind==4){
            if(debug){
                cout << "relexpAST, kind 4" <<endl;
            }
            return generate_return();
        }
        else{
            assert(0);
        }
    }
};

class PrimaryExpAST:public BaseAST{
    public:
    int kind;
    unique_ptr<BaseAST> exp;//kind = 0
    unique_ptr<BaseAST> number;//kind = 1

    KIR Dump() const override {
        if(debug){
            cout<<"PrimaryExpAST, kind "<< kind <<endl;
        }
        if(kind == 0){
            return exp->Dump();
        }
        else if(kind == 1){
            return number->Dump();
        }
        else{
            assert(0);
        }
    }
};

class AddExpAST : public BaseAST{
    public:
        unique_ptr<BaseAST> add_exp;
        unique_ptr<BaseAST> mul_exp;
        int kind;//0 mul,1 + ,2 -
        KIR generate_return() const {
            KIR exp_dump_1 = add_exp->Dump();
            string tmp_cmd_1 = exp_dump_1.s;
            string tmp_var_1 = exp_dump_1.tmp_var;
            KIR exp_dump_2 = mul_exp->Dump();
            string tmp_cmd_2 = exp_dump_2.s;
            string tmp_var_2 = exp_dump_2.tmp_var;
            string tmp_var_new = "%" + to_string(tmp_var_cnt++);
            if(debug){
                cout << "tmp_cmd_1: " << tmp_cmd_1 << endl;
                cout << " tmp_var_1: " << tmp_var_1 << endl;
                cout << "tmp_cmd_2: " << tmp_cmd_2 << endl;
                cout << "tmp_var_2: " << tmp_var_2 << endl;
                cout << "tmp_var_new: " << tmp_var_new << endl;
            }
            string op = " = add ";
            if(kind==1) op = " = add ";
            else if(kind == 2) op = " = sub ";
            return KIR(tmp_cmd_1 + tmp_cmd_2 + " " + tmp_var_new + op + tmp_var_1 + ", " + tmp_var_2 + "\n",tmp_var_new);
        }
        KIR Dump() const override{
            if(debug){
                cout<<"addexpAST, kind "<< kind <<endl;
            }
            if(kind == 0){
                return mul_exp->Dump();
            }
            else if(kind==1){
                return generate_return();
            }
            else if(kind == 2){
                return generate_return();
            }
            else{
                assert(false);
            }
        }
};

class MulExpAST : public BaseAST{
    public:
        int kind;
        //0 unary, 1 * ,2 / ,3 %
        unique_ptr<BaseAST> mul_exp;
        unique_ptr<BaseAST> unary_exp;
        KIR generate_return()const{
            KIR exp_dump_1 = mul_exp->Dump();
            string tmp_cmd_1 = exp_dump_1.s;
            string tmp_var_1 = exp_dump_1.tmp_var;
            KIR exp_dump_2 = unary_exp->Dump();
            string tmp_cmd_2 = exp_dump_2.s;
            string tmp_var_2 = exp_dump_2.tmp_var;
            string tmp_var_new = "%" + to_string(tmp_var_cnt++);
            if(debug){
                cout << "tmp_cmd_1: " << tmp_cmd_1 << endl;
                cout << " tmp_var_1: " << tmp_var_1 << endl;
                cout << "tmp_cmd_2: " << tmp_cmd_2 << endl;
                cout << "tmp_var_2: " << tmp_var_2 << endl;
                cout << "tmp_var_new: " << tmp_var_new << endl;
            }
            string op = " = mul ";
            if(kind==1) op = " = mul ";
            else if(kind == 2) op = " = div ";
            else if(kind==3) op = " = mod ";
            return KIR(tmp_cmd_1 + tmp_cmd_2 + " " + tmp_var_new + op + tmp_var_1 + ", " + tmp_var_2 + "\n",tmp_var_new);
        }
        KIR Dump()const override{
            if(debug){
                cout<<"mulexp"<<endl;
            }
            if(kind==0){
                return unary_exp->Dump();
            }
            else if(kind==1){
                return generate_return();
            }
            else if(kind==2){
                return generate_return();
            }
            else if(kind==3){
                return generate_return();
            }
            else{
                assert(0);
            }
        }
};

class UnaryExpAST : public BaseAST{
    public:
        int kind;
        //0 prime
        //1 op exp
        unique_ptr<BaseAST> primary_exp;
        unique_ptr<BaseAST> unary_exp;
        unique_ptr<BaseAST> unary_op;
        KIR Dump()const override{
            if(kind==0){
                if(debug){
                    cout<<"UnaryAST,kind 0"<<endl;
                }
                return primary_exp->Dump();
            }
            else if(kind==1){
                if(debug){
                    cout<<"UnaryAST,kind 1"<<endl;
                }
                string op = unary_op->Dump().tmp_var;
                KIR exp_dump = unary_exp->Dump();
                string tmp_cmd = exp_dump.s;
                string tmp_var = exp_dump.tmp_var;
                string tmp_var_new = "%" + to_string(tmp_var_cnt++);
                if(debug){
                    cout<<"op: "<< op <<endl;
                    cout<<"tmp_cmd: "<<tmp_cmd<<endl;
                    cout<<"tmp_var: "<<tmp_var<<endl;
                    cout<<"tmp_var_new: "<<tmp_var_new<<endl;
                }
                if(op == "!"){
                    return KIR(tmp_cmd + " " + tmp_var_new + " = eq " + tmp_var +", 0 \n",tmp_var_new);
                }
                else if(op== "-"){
                    return KIR(tmp_cmd + " " + tmp_var_new + " = sub 0, " + tmp_var + "\n",tmp_var_new);
                }
                else if(op == "+"){
                    return exp_dump;
                }
                else{
                    cout<<"unknown op"<<endl;
                    assert(0);
                }
            }
            else{
                assert(0);
            }
        }
};

class UnaryOpAST: public BaseAST{
    public:
    int kind;//0 +, 1 -,2 !
    string op;
    KIR Dump()const override{
        return KIR("",op);
    }
};

class NumberAST : public BaseAST {
public:
    // -- INT_CONST
    int int_const = 0;
    NumberAST() = default;
    NumberAST(int _int_const) : int_const(_int_const) {}

    KIR Dump() const override {
        string s = to_string(int_const);
        return KIR("",s);
    }
};

// ...

