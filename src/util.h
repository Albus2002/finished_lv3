#pragma once
#include<iostream>
#include<cstdio>
#include<cstring>
#include"koopa.h"

using namespace std;

static int debug = 0;//调试模式
static int tmp_var_cnt = 0;//临时变量标号
static int basic_block_cnt = 0;//基本块数量
static int block_cnt = 0;//block数量

// 所有 AST 的基类
class KIR{
    public:
    string s = "";
    string type = "i32";
    string tmp_var = "";
    int end = 0;

    KIR(){}
    KIR(string _s){this->s = _s;}
    KIR(string s,string Var){
      this->s = s;
      this->tmp_var = Var;
    }
    KIR(string s, int _end){
      this->s = s;
      this->end = _end;
    }
};

class R_code{
public:
  string code = "";
  R_code(){}
  R_code(string _s){
    this->code = _s;
  }
};

class R_Info{
  public:
    string func_name = "";
    int stack_bio = 0;
    int stack_szie = 0;
    //string reg = "t0";
    koopa_raw_slice_t func_param;
    koopa_raw_slice_t bb_param;
    R_Info(){};
};

class BaseAST {
 public:
  virtual ~BaseAST() = default;
  virtual KIR Dump() const = 0;
};

static string new_label(){
  return "%Label_" + to_string(basic_block_cnt++);
}