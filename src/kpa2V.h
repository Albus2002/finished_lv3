#pragma once
#include "ast.h"
#include "util.h"
#include<iostream>
#include<cstring>

using namespace std;


// 函数声明
R_code Visit(const koopa_raw_program_t& program);
R_code Visit(const koopa_raw_slice_t &slice,const R_Info& info);
R_code Visit(const koopa_raw_function_t &func,const R_Info& info);
R_code Visit(const koopa_raw_basic_block_t &bb,const R_Info& info);
R_code Visit(const koopa_raw_value_t &value,const R_Info& info);
R_code Visit(const koopa_raw_return_t& ret,const R_Info& info);
R_code Visit(const koopa_raw_integer_t& integer, const R_Info& info);
string KIR_RISCV(const char* str);




// 访问 raw program
R_code Visit(const koopa_raw_program_t &program) {
  // 执行一些其他的必要操作
  // ...
  R_Info info;
  // 访问所有全局变量
  R_code values_code = Visit(program.values,info);
  if(debug){
      cout<< "global sucess\n";
    }
  // 访问所有函数
  R_code funcs_code = Visit(program.funcs,info);
  if(debug){
      cout<< "func sucess\n";
    }

  return R_code(values_code.code + funcs_code.code);
}

// 访问 raw slice
R_code Visit(const koopa_raw_slice_t &slice,const R_Info& info) {
  R_Info nextInfo = info;
  string ret = "";
  if(debug){
        cout<< "slice kind: "<<slice.kind<<endl;
    }
  for (size_t i = 0; i < slice.len; ++i) {
    auto ptr = slice.buffer[i];
    // 根据 slice 的 kind 决定将 ptr 视作何种元素
    switch (slice.kind) {
      case KOOPA_RSIK_FUNCTION:{// 访问函数
        if(debug){
                cout<< "func translate\n";
        }
        auto func_ptr = reinterpret_cast<koopa_raw_function_t>(ptr);
        ret.append(Visit(func_ptr,nextInfo).code);
        
        break;
        }
      case KOOPA_RSIK_BASIC_BLOCK:{
        // 访问基本块
        if(debug){
                cout<< "bb translate\n";
        }
        auto block_ptr = reinterpret_cast<koopa_raw_basic_block_t>(ptr);
        ret.append(Visit(block_ptr,nextInfo).code);
        break;
        }
      case KOOPA_RSIK_VALUE:{// 访问指令
        if(debug){
            cout<< "risc_val translate\n";
        }
        auto value_ptr = reinterpret_cast<koopa_raw_value_t>(ptr);
        ret.append(Visit(value_ptr,nextInfo).code);
        break;
        }
      default:
        // 我们暂时不会遇到其他内容, 于是不对其做任何处理
        assert(false);
    }
  }
  return R_code(ret);
}

// 访问函数
R_code Visit(const koopa_raw_function_t &func,const R_Info& info) {
  // 执行一些其他的必要操作
  // ...
  string ret = "\n  .text\n  .globl ";
  R_Info nextInfo;
  const char* func_name = (const char*)(func->name + 1);
  //跳过开头的@
  ret.append(func_name);
  ret.append("\n");
  ret.append(func_name);
  ret.append(":\n");

  // 访问所有基本块
  if(debug){
        cout<< "bbs translate\n";
    }
  ret.append(Visit(func->bbs,nextInfo).code);
  
  return R_code(ret);
}

// 访问基本块
R_code Visit(const koopa_raw_basic_block_t &bb,const R_Info& info) {
  // 执行一些其他的必要操作
  // ...
  const char* bname = bb->name;
  // 访问所有指令
  string ret = "";
  if(debug){
        cout<< "insts translate\n";
    }
  ret = Visit(bb->insts,info).code;
  if(strcmp(bb->name,"%entry")==0){

  }
  return R_code(ret);
}

// 访问指令
R_code Visit(const koopa_raw_value_t &value,const R_Info& info) {
  // 根据指令类型判断后续需要如何访问
  const auto &kind = value->kind;
  if(debug){
    cout<<"value kind: "<<kind.tag<<endl;
  }
  R_code tmp = R_code("");
  switch (kind.tag) {
    case KOOPA_RVT_RETURN:{// 访问 return 指令
      if(debug){
            cout<<"return cmd\n";
        }
      tmp = Visit(kind.data.ret,info);
      break;
    }
    case KOOPA_RVT_INTEGER:{// 访问 integer 指令
      if(debug){
            cout<<"integer cmd\n";
        }
      tmp = Visit(kind.data.integer,info);
      break;
    }
    default:
      // 其他类型暂时遇不到
      assert(false);
  }
  return tmp;
}

R_code Visit(const koopa_raw_return_t& ret,const R_Info& info){
    if(debug){
        cout<<"raw_return start.\n";
    }
    string s = "";
    if(ret.value != nullptr){
        R_code tmp = Visit(ret.value,info);
        s += tmp.code;
    }
    s.append("  ret\n");
    if(debug){
        cout<<"raw_return return.\n";
    }
    return R_code(s);
}

R_code Visit(const koopa_raw_integer_t& integer, const R_Info& info){
    if(debug){
        cout<<"raw_integer start.\n";
    }
    string val = to_string(integer.value);
    string s = "  li a0, " + val + "\n";
    if(debug){
        cout<<"raw_integer return.\n";
    }
    return R_code(s);
}
// 访问对应类型指令的函数定义略
// 视需求自行实现
// ...


string KIR_RISCV(const char* str){
    // 解析字符串 str, 得到 Koopa IR 程序
    koopa_program_t program;
    koopa_error_code_t ret = koopa_parse_from_string(str, &program);
    assert(ret == KOOPA_EC_SUCCESS);  // 确保解析时没有出错
    // 创建一个 raw program builder, 用来构建 raw program
    koopa_raw_program_builder_t builder = koopa_new_raw_program_builder();
    if(debug){
      cout<< "raw program build sucess\n";
    }
    // 将 Koopa IR 程序转换为 raw program
    koopa_raw_program_t raw = koopa_build_raw_program(builder, program);
    if(debug){
      cout<< "raw program translate sucess\n";
    }
    // 释放 Koopa IR 程序占用的内存
    koopa_delete_program(program);

    // 处理 raw program
    // ...
    R_code riscv = Visit(raw);
    if(debug){
      cout<< "visit sucess\n";
    }

    // 处理完成, 释放 raw program builder 占用的内存
    // 注意, raw program 中所有的指针指向的内存均为 raw program builder 的内存
    // 所以不要在 raw program 处理完毕之前释放 builder
    koopa_delete_raw_program_builder(builder);
    return riscv.code;

}


